/*
 *   Copyright (c) 2018.
 *   This file is part of NeGraph.
 *
 *  NeGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NeGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NeGraph.  If not, see <https://www.gnu.org/licenses/>.
 * @author Jyothish Soman, cl cam uk
 */

package org.cam.storage.levelgraph;

import org.cam.storage.levelgraph.datatypes.Direction;
import org.cam.storage.levelgraph.datatypes.Edge;
import org.cam.storage.levelgraph.datatypes.LevelNode;
import org.cam.storage.levelgraph.storage.RocksDBInterface;
import org.cam.storage.levelgraph.storage.ondiskstorage.FileStorageLayerInterface;
import org.cam.storage.levelgraph.storage.ondiskstorage.StorageReaderWriter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/*

This test will fail if the hardcoded on-disk size of primitives is not the same as the actual.

*/

public class StorageReaderWriterTest {
    private static StorageReaderWriter storageReaderWriter;
    private static String fileName;


    @BeforeClass
    public static void writeFile(){
        FileStorageLayerInterface fileStorageLayerInterface = new FileStorageLayerInterface("fileStore/dataStore");
        RocksDBInterface test = mock(RocksDBInterface.class);
        when(test.getValue((long) 0, 4)).thenReturn((long) 0);
        fileStorageLayerInterface.setRocksDBInterface(test);
        UpdatesCache updatesCache = new UpdatesCache(fileStorageLayerInterface);
        updatesCache.addNode(new LevelNode(0));
        updatesCache.addNode(new LevelNode(1));
        updatesCache.addEdge((long) 0,new Edge((long)1,0,new Direction(Direction.DirectionValues.Bidirectional), null) );
        updatesCache.addEdge((long) 1,new Edge((long)0,1,new Direction(Direction.DirectionValues.Bidirectional), null) );
        fileName=updatesCache.flushToDisk();
        storageReaderWriter = new StorageReaderWriter(fileName);
        System.out.println("NodeCount:" + storageReaderWriter.getNodeCount());
    }
    static void deleteDir(File file) {
        File[] contents = file.listFiles();
        if (contents != null) {
            for (File f : contents) {
                deleteDir(f);
            }
        }
        file.delete();
    }

    static void deleteFile(String fileName){
        File file=new File(fileName);
        deleteDir(file);
    }
    @AfterClass
    public static void deleteFile(){
        System.out.println(fileName);
        deleteFile(fileName);
        deleteFile("fileStore");
    }

    @Test
    public void getNeighbour() throws Exception {
        assertEquals(storageReaderWriter.getNeighbour(0, 0), 1);
        assertEquals(storageReaderWriter.getNeighbour(1, 0), 0);
    }

    @Test
    public void getEdgeDirection() throws Exception {
        assertEquals(storageReaderWriter.getEdgeDirection(0, 0).getDirectionValue(), Direction.DirectionValues.Bidirectional);
        assertEquals(storageReaderWriter.getEdgeDirection(1, 0).getDirectionValue(), Direction.DirectionValues.Bidirectional);
    }

    @Test
    public void getEdgeId() throws Exception {
        assertEquals(storageReaderWriter.getEdgeId(0, 0), 0);
        assertEquals(storageReaderWriter.getEdgeId(1, 0), 1);
    }

    @After
    public void testDeleteEdge() throws Exception {
        storageReaderWriter.deleteEdge(new Edge((long) 0, (long) 1, (long) -1, new Direction(Direction.DirectionValues.Bidirectional), null));
        ArrayList<Edge> array = storageReaderWriter.getEdges(0);
        System.out.println(array.size());
    }

}