/*
 *   Copyright (c) 2018.
 *   This file is part of NeGraph.
 *
 *  NeGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NeGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NeGraph.  If not, see <https://www.gnu.org/licenses/>.
 * @author Jyothish Soman, cl cam uk
 */

package org.cam.storage.levelgraph.datatypes;

import org.cam.storage.levelgraph.dataUtils.PrimitiveDeserialiser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EdgeTest {
    Edge edge;

    @Before
    public void setUp() throws Exception {
        edge = new Edge(4, 5, 0, '2', null);

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void toBytes() throws Exception {
        byte[] bytes = edge.toBytes();
        int length = (int) bytes[0];
        assertEquals(26, length);
        int index = 1;
        long neighbour = PrimitiveDeserialiser.getInstance().longFromBytes(bytes[index], bytes[index + 1], bytes[index + 2], bytes[index + 3], bytes[index + 4], bytes[index + 5], bytes[index + 6], bytes[index + 7]);
        assertEquals(neighbour, 5);
        index = 9;
        long nodeId = PrimitiveDeserialiser.getInstance().longFromBytes(bytes[index], bytes[index + 1], bytes[index + 2], bytes[index + 3], bytes[index + 4], bytes[index + 5], bytes[index + 6], bytes[index + 7]);
        assertEquals(nodeId, 4);
        index += 8;
        long edgeId = PrimitiveDeserialiser.getInstance().longFromBytes(bytes[index], bytes[index + 1], bytes[index + 2], bytes[index + 3], bytes[index + 4], bytes[index + 5], bytes[index + 6], bytes[index + 7]);
        assertEquals(edgeId, 0);
        index += 8;
        Direction direction = new Direction((char) bytes[index]);
        assertEquals((char) direction.getDirection(), '2');
    }


}