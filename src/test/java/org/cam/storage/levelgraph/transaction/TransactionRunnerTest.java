/*
 *   Copyright (c) 2018.
 *   This file is part of NeGraph.
 *
 *  NeGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NeGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NeGraph.  If not, see <https://www.gnu.org/licenses/>.
 * @author Jyothish Soman, cl cam uk
 */

package org.cam.storage.levelgraph.transaction;

import org.cam.storage.levelgraph.Pair;
import org.cam.storage.levelgraph.storage.IdGenerator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TransactionRunnerTest {
    Transaction transaction;
    TransactionRunner transactionRunner;

    @Before
    public void setUp() throws Exception {
        IdGenerator idgen1=mock(IdGenerator.class);
        IdGenerator idgen2=mock(IdGenerator.class);
        when(idgen1.getIdRange()).thenReturn(new Pair<>((long)0,(long)100));
        when(idgen2.getIdRange()).thenReturn(new Pair<>((long)0,(long)100));
        transaction = new Transaction(new TransactionScheduler());
        transactionRunner = new TransactionRunner(transaction,idgen1,idgen2);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void addEdge() throws Exception {
    }

    @Test
    public void addNode() throws Exception {
    }

    @Test
    public void getEdges() throws Exception {
    }

    @Test
    public void deleteNode() throws Exception {
    }

    @Test
    public void findNode() throws Exception {
    }

    @Test
    public void deleteEdge() throws Exception {
    }

    @Test
    public void getTransactionId() throws Exception {
    }

    @Test
    public void getResults() throws Exception {
    }

    @Test
    public void executeTransaction() throws Exception {
    }

}