/*
 *   Copyright (c) 2018.
 *   This file is part of NeGraph.
 *
 *  NeGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NeGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NeGraph.  If not, see <https://www.gnu.org/licenses/>.
 * @author Jyothish Soman, cl cam uk
 */

package org.cam.storage.levelgraph.transaction;

import org.cam.storage.levelgraph.Pair;
import org.cam.storage.levelgraph.datatypes.Edge;
import org.cam.storage.levelgraph.storage.IdGenerator;
import org.cam.storage.levelgraph.storage.dataqueue.WriteAheadLog;
import org.cam.storage.levelgraph.storage.ondiskstorage.UnifiedDataStorageBuilder;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TransactionalStorageTest {
//TODO: Need to complete this test.

    private static TransactionalStorage transactionalStorage;

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @BeforeClass
    public static void setUp() {

    }

    @AfterClass
    public static void tearDown() {
    }

    @Test
    public void addEdge() {
        IdGenerator idgen1=mock(IdGenerator.class);
        IdGenerator idgen2=mock(IdGenerator.class);
        when(idgen1.getIdRange()).thenReturn(new Pair<>((long)0,(long)100));
        when(idgen2.getIdRange()).thenReturn(new Pair<>((long)0,(long)100));
        transactionalStorage = new TransactionalStorage(idgen1,idgen2);
        String path=folder.newFolder("filestorage").getPath();
        transactionalStorage.setStorage(
                new UnifiedDataStorageBuilder()
                        .setPath(path)
                        .createUnifiedDataStorage());
        transactionalStorage.setWal(new WriteAheadLog(path+"/wal"));
        transactionalStorage.addEdge(new Edge(0, 1, 0, '2', null));
        transactionalStorage.commit();
    }

    @Test
    public void addNode() {

    }

    @Test
    public void getEdges() {

    }

    @Test
    public void deleteNode() {

    }

    @Test
    public void findNode() {

    }

    @Test
    public void deleteEdge() {

    }

    @Test
    public void getTransactionId() {

    }

    @Test
    public void getResults() {

    }

    @Test
    public void setTransaction() {
    }

    @Test
    public void commit() {
    }
}