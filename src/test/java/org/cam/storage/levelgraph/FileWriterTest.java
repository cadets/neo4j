/*
 *   Copyright (c) 2018.
 *   This file is part of NeGraph.
 *
 *  NeGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NeGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NeGraph.  If not, see <https://www.gnu.org/licenses/>.
 * @author Jyothish Soman, cl cam uk
 */

package org.cam.storage.levelgraph;

import org.cam.storage.levelgraph.storage.FileWriter;
import org.cam.storage.levelgraph.storage.FileWriterBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class FileWriterTest {
    FileWriter fileWriter;
    private static String fileName="level0.0.dat";
    @Before
    public void initialise(){
        fileWriter = new FileWriterBuilder().setFileName(fileName).createFileWriter();
        fileWriter.writeToFile(null, null);
    }
    @Test
    public void writeToFile() throws Exception {
        FileChannel fileChannel = new RandomAccessFile(new File(fileName), "rw").getChannel();
        long fileSize = fileChannel.size();
        MappedByteBuffer mbuffer = fileChannel.map(FileChannel.MapMode.READ_WRITE, 0, fileSize);
        System.out.println(mbuffer.getLong());
        System.out.println(mbuffer.getLong());
        System.out.println(mbuffer.getLong());
    }
    @After
    public void cleanUp(){
        (new File(fileName)).delete();
        (new File(fileName+".bloom")).delete();
    }

}