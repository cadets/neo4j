/*
 *   Copyright (c) 2018.
 *   This file is part of NeGraph.
 *
 *  NeGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NeGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NeGraph.  If not, see <https://www.gnu.org/licenses/>.
 * @author Jyothish Soman, cl cam uk
 */

package org.cam.storage.levelgraph;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class MMapTest {
    private MappedByteBuffer mappedByteBuffer;
    @Before
    public void initialise(){
        try {
            String filename="TestFile.dat";
            FileChannel fileChannel = new RandomAccessFile(new File(filename), "rw").getChannel();
            long fileSize = 100;
            mappedByteBuffer = fileChannel.map(FileChannel.MapMode.READ_WRITE, 0, fileSize);
            mappedByteBuffer.putLong(10);
            mappedByteBuffer.putLong(11);
            mappedByteBuffer.putLong(12);
            System.out.println(mappedByteBuffer.position());
            mappedByteBuffer.force();
            fileChannel.close();
        }catch (Exception e){
            System.err.println(e.toString());
        }
    }
    @Test
    public void MMapTest(){
         try {
            String filename="TestFile.dat";
            FileChannel fileChannel = new RandomAccessFile(new File(filename), "r").getChannel();
            long fileSize = 100;
            mappedByteBuffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, fileSize);
            System.out.println(mappedByteBuffer.getLong());
            System.out.println(mappedByteBuffer.getLong());
            System.out.println(mappedByteBuffer.getLong());
            fileChannel.close();
        }catch (Exception e){
            System.err.println(e.toString());
        }

    }
    static void deleteDir(File file) {
        File[] contents = file.listFiles();
        if (contents != null) {
            for (File f : contents) {
                deleteDir(f);
            }
        }
        file.delete();
    }
    @After
    public void cleanup(){
        deleteDir(new File("TestFile.dat"));
    }
}
