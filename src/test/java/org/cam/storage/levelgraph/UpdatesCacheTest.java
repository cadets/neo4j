/*
 *   Copyright (c) 2018.
 *   This file is part of NeGraph.
 *
 *  NeGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NeGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NeGraph.  If not, see <https://www.gnu.org/licenses/>.
 * @author Jyothish Soman, cl cam uk
 */

package org.cam.storage.levelgraph;

import org.cam.storage.levelgraph.datatypes.Direction;
import org.cam.storage.levelgraph.datatypes.Edge;
import org.cam.storage.levelgraph.storage.ondiskstorage.FileStorageLayerInterface;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class UpdatesCacheTest {
    UpdatesCache updatesCache;
    FileStorageLayerInterface fileStorageLayerInterface;
    @Rule
    public TemporaryFolder temporaryFolder=new TemporaryFolder();
    @Before
    public void initialise(){

        fileStorageLayerInterface = new FileStorageLayerInterface(temporaryFolder.newFolder("fileStore").getPath());
        updatesCache = new UpdatesCache(fileStorageLayerInterface);
        updatesCache.addNode((long)0);
        updatesCache.addNode((long) 1);
        updatesCache.addEdge((long) 0,new Edge((long)1,-1,new Direction(Direction.DirectionValues.Bidirectional), null) );
        updatesCache.addEdge((long) 1,new Edge((long)0,-1,new Direction(Direction.DirectionValues.Bidirectional), null) );
    }
    @Test
    public void getCurrentNodeId() throws Exception {
    }


    @Test
    public void getCurrentEdgeId() throws Exception {
    }



    @Test
    public void getProperties() throws Exception {
    }

    @Test
    public void getProperties1() throws Exception {
    }

    @Test
    public void deleteEdge() throws Exception {
    }



    @Test
    public void getEdges() throws Exception {
        Edge [] edges;
        edges = new Edge[1];
        edges[0]=new Edge((long)1,-1,new Direction(Direction.DirectionValues.Bidirectional), null) ;
        for (Edge e: updatesCache.getEdges((long)0)
             ) {
            System.out.println(e.getNeighbour());

        }
        assertArrayEquals(updatesCache.getEdges((long)0).toArray(),edges);
    }

    @Test
    public void getEdges1() throws Exception {
    }

}