/*
 *   Copyright (c) 2018.
 *   This file is part of NeGraph.
 *
 *  NeGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NeGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NeGraph.  If not, see <https://www.gnu.org/licenses/>.
 * @author Jyothish Soman, cl cam uk
 */

package org.cam.storage.levelgraph;

import org.cam.storage.levelgraph.datatypes.LevelNode;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LevelNodeTest {
    LevelNode levelNode;
    @Before
    public void initialise(){
        levelNode = new LevelNode(1);
        levelNode.setInternalId((long) 2);
        levelNode.setExternalId((long)1);
        levelNode.setCreatingTransaction((long) 1);
        levelNode.setDeletingTransaction((long) -1);
    }

    @Test
    public void getExternalId() throws Exception {
        assertEquals((long) levelNode.getExternalId(), 1);
    }

    @Test
    public void getCreatingTransaction() throws Exception {
        assertEquals((long) levelNode.getCreatingTransaction(), 1);
    }


    @Test
    public void getDeletingTransaction() throws Exception {
        assertEquals((long) levelNode.getDeletingTransaction(), -1);
    }


    @Test
    public void getInternalId() throws Exception {
        assertEquals((long) levelNode.getInternalId(), 2);
    }

    @Test
    public void serialisationTest() throws Exception {
        LevelNode newLevelNode = new LevelNode(levelNode.toBytes());
        assertEquals(levelNode.getInternalId(), newLevelNode.getInternalId());
        assertEquals(levelNode.getExternalId(), newLevelNode.getExternalId());
    }
}