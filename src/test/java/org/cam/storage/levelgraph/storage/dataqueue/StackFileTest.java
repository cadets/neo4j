/*
 *   Copyright (c) 2018.
 *   This file is part of NeGraph.
 *
 *  NeGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NeGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NeGraph.  If not, see <https://www.gnu.org/licenses/>.
 * @author Jyothish Soman, cl cam uk
 */

package org.cam.storage.levelgraph.storage.dataqueue;

import com.google.common.primitives.Longs;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class StackFileTest {
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();


    @Test
    public void add() throws IOException {
        StackFile stackFile, stackFile1;
        File tempFolder = folder.newFolder("tempFolder");
        if (!tempFolder.exists())
            tempFolder.mkdirs();
        stackFile = new StackFile.StackBuilder(folder.newFile("tempFolder/temp")).build();

        try {
            stackFile.add(Longs.toByteArray(123));
            assertEquals(Longs.fromByteArray(stackFile.peek()), 123);

            stackFile.add(Longs.toByteArray(124));
            assertEquals(Longs.fromByteArray(stackFile.peek()), 124);
            stackFile.remove();
            assertEquals(Longs.fromByteArray(stackFile.peek()), 123);
        } catch (IOException e) {
        }
        stackFile1 = new StackFile.StackBuilder(folder.newFile("tempFolder/temp")).build();

        /*
         * Test if the file reader part works correctly.
         */

        assertEquals(stackFile1.size(), stackFile.size());
    }

    @Test
    public void size() {
    }

    @Test
    public void remove() {
    }

    @Test
    public void clear() {
    }
}