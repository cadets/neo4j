/*
 *   Copyright (c) 2018.
 *   This file is part of NeGraph.
 *
 *  NeGraph is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NeGraph is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NeGraph.  If not, see <https://www.gnu.org/licenses/>.
 * @author Jyothish Soman, cl cam uk
 */

package org.cam.storage.levelgraph.storage;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;

import static org.junit.Assert.assertEquals;

public class RocksDBInterfaceTest {
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();
    static RocksDBInterface rocksDBInterface;
    @BeforeClass
    public static void initialise(){

    }
    @Test
    public void getStringValue() {
        File testDirectory = folder.newFolder("dbfolder");
        String path = testDirectory.getPath();
        rocksDBInterface = new RocksDBInterface(path, 1);
        rocksDBInterface.setValue((long) 1, (long) 23, 0);
        rocksDBInterface.setValue("Key", (long) 34, 0);
        rocksDBInterface.setValue((long) 2, "Aux", (long) 91, 0);
        assertEquals((long) rocksDBInterface.getValue("Key", 0), (long) 34);
        assertEquals((long) rocksDBInterface.getValue((long) 1, 0), (long) 23);
        assertEquals((long) rocksDBInterface.getValue((long) 2, "Aux", 0), (long) 91);
        folder.delete();
    }


}