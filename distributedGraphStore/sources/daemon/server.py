import Queue
import socket

class Server(object):
    def __init__(self,config):
        self.name="Server"
        self.readQueue=Queue.Queue()
        self.writeQueue=Queue.Queue()
        self.siblings=0
        self.children=0
        self.startServer()
        self.s=socket.socket()
        host = socket.gethostname()
        port = config.get('server','port')
        print host, port
        self.s.bind((host, int(port)))

    def startRemoteChildServer(self):
        print"Starting remote child server"
    def startRemoteSiblingServer(self,siblingId):
        print"Starting Sibling Server"

    def startServer(self):
        for node in range(self.siblings):
            self.startRemoteSiblingServer(node)
        for node in range(self.children):
            self.startRemoteChildServer(node)



        print "Start server"
    def stopServer(self):
        print "Stop Server"
    def addNode(self,nodeInfo):
        print"Add Node"
    def addEdge(self,edgeInfo):
        print "Adding edge"
    def findNodeGroup(self,node):
        return 0
    def postQuery(self,query):
        print"Query"


    def getData(self):
        while True:
            self.s.listen(5)
            try:
                c,addr =self.s.accept()
                c.send("Hakuna matata")
                c.close()
            except socket.error,msg:
                print socket.error, msg





# Server would have the following:
# Multiple threads
# Each thread would be specialised.
