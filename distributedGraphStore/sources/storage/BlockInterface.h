/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BlockInterface.h
 * Author: jyothish
 *
 * Created on 03 July 2018, 07:35
 */

#ifndef BLOCKINTERFACE_H
#define BLOCKINTERFACE_H
#include"storage/BlockStorage.hh"
#include<unordered_set>
#include <memory>

/*
 * Block interface does not do additions, they are to be merged together into a block addition. 
 * The only operations that can be done are reads, updates and deletes. 
 * Updates on edges are a bit more interesting. 
 */


class BlockInterface {
protected:

    typedef struct {
        int firstOccurence, lastOccurence;
        int firstSubIndex, lastSubIndex;
    } nodeData;
    
    std::unordered_map<int, std::shared_ptr<BlockStorage>> blocks;

    int counter;

    std::unordered_map<int, nodeData> nodeMap;

public:

    BlockInterface();

    void addBlock(int n, int m, std::unordered_map<int, std::vector<int>> graphChunk,int tid) ;

    std::vector<int> getEdges(int n) ;

    
    std::vector<int> getNodes();

    int findAnyNode(std::unordered_set<int> nodeSet, int n) ;


    bool deleteEdge(int n, int to) ;


    void mergeBlocks(int id, int id2) ;


    void deleteNode(int n) ;

     
    bool nodeAvailable(int node);

};



#endif /* BLOCKINTERFACE_H */

