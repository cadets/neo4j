#ifndef __nodeDB__hh__
#define __nodeDB__hh__

#include<cstdio>
#include<vector>
#include<string>
#include<map>
#include<atomic>
#include<memory>
#include<iostream>
#include"common/commonDefs.hh"
#include"storage/edgeHandles.hh"
#include"storage/properties.hh"

using namespace std;

class NodeDbDriver{

    vector<shared_ptr<EdgeListHead>> adjacencyList;

    map<lli,lli> physicalToLogicalMap;

    lli capacity, filled;

    std::atomic_flag lock;
  
    
    lli getId(lli id){
        if(physicalToLogicalMap.find(id)!=physicalToLogicalMap.end()){
            return physicalToLogicalMap[id];
        }
        return 0;
    }
    
    shared_ptr<EdgeListHead> getAdjacency(lli node);
    
    public:
    
    NodeDbDriver(lli n);

    void deleteNode(lli node);   
    
    lli addNode(lli node,lli transactionId);

    lli addEdge(lli from, lli to, lli cid);

    lli getNodeId(lli node);

    lli removeNode(lli node, lli cid);

    void removeEdge(lli node, EdgeList* edge, lli cid);

    lli deleteEdge(lli from, lli to, lli cid);

    vector<lli> getNeighbours(lli node, lli tid){
        vector<lli> results;
        lli nodeId=getId(node);
        cout<<nodeId<<" "<<node<<endl;
        if(nodeId==-1||adjacencyList[nodeId]==NULL){
            cout<<"Trapped"<<endl;
            return results;
        }
        return adjacencyList[nodeId]->getNeighbours(tid);
    }
};

class FileNodeDbDriver{

    /* NodeIndex* nodeIndex; */

    public:

    FileNodeDbDriver(lli n);

    void deleteNode(lli node);
    
    lli addNode(lli node,lli transactionId);

    void addEdge(lli from, lli to, lli cid);

    void addEdge(lli from, EdgeList* edge, lli transactionId);

    lli getNodeId(lli node, lli tid);

    void removeNode(lli node, lli tid);

    void removeEdge(lli node, EdgeList* edge, lli tid);

    void removeEdge(lli to, lli from, lli tid);
};

typedef int Node;

#endif
