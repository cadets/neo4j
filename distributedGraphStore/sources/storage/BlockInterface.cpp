//
// Created by jyothish on 13/11/18.
//

#include "BlockInterface.h"

    BlockInterface::BlockInterface(){
        counter=0;
    }

    void BlockInterface::addBlock(int n, int m, std::unordered_map<int, std::vector<int>> graphChunk,int tid) {
        //The following is a concurrency issue.
        blocks[counter] = std::make_shared<BlockStorage>(n, m, graphChunk,tid);
        int blockId = counter++;
        std::vector<std::pair<int, int>> nodeIndex = blocks[blockId]->getNodesInBlockWithIndex();
        for (auto x : nodeIndex) {
            if (nodeMap.find(x.first) == nodeMap.end()) {
                nodeMap[x.first].firstOccurence = blockId;
                nodeMap[x.first].firstSubIndex = x.second;
            } else {
                blocks[nodeMap[x.first].lastOccurence]->addNextIndex(x.first, nodeMap[x.first].lastSubIndex, blockId, x.second);
                blocks[blockId]->addPrevIndex(x.first, x.second, nodeMap[x.first].lastOccurence, nodeMap[x.first].lastSubIndex);
            }
            nodeMap[x.first].lastSubIndex = x.second;
            nodeMap[x.first].lastOccurence = blockId;
        }
    }

    std::vector<int> BlockInterface::getEdges(int n) {
        std::vector<int> result;
        if (nodeMap.find(n) != nodeMap.end()) {
            int blockid = nodeMap[n].firstOccurence;
            int blocksubIndex = nodeMap[n].firstSubIndex;
            while (blockid != -1) {
                std::vector<int> edgeList = blocks[blockid]->getEdges(n, blocksubIndex);
                result.insert(result.end(), edgeList.begin(), edgeList.end());
                std::pair<int, int> nextIndexes = blocks[blockid]->getNextIndexes(n, blocksubIndex);
                blockid = nextIndexes.first;
                blocksubIndex = nextIndexes.second;
            }

        }
        return result;
    }

    std::vector<int> BlockInterface::getNodes(){

        std::vector<int> nodes;
        nodes.reserve(nodeMap.size());
        for(auto const& imap: nodeMap)
            nodes.push_back(imap.first);
        return nodes;
    }

    int BlockInterface::findAnyNode(std::unordered_set<int> nodeSet, int n) {
        if (nodeMap.find(n) != nodeMap.end()) {
            int blockid = nodeMap[n].firstOccurence;
            int blocksubIndex = nodeMap[n].firstSubIndex;
            while (blockid != -1) {
                std::vector<int> edgeList = blocks[blockid]->getEdges(n, blocksubIndex);
                for (auto x : edgeList) {
                    if (nodeSet.find(x) != nodeSet.end()) {
                        return x;
                    }
                }
                std::pair<int, int> nextIndexes = blocks[blockid]->getNextIndexes(n, blocksubIndex);
                blockid = nextIndexes.first;
                blocksubIndex = nextIndexes.second;
            }

        }
        return -1;
    }

    bool BlockInterface::deleteEdge(int n, int to) {
        if (nodeMap.find(n) != nodeMap.end()) {
            int blockid = nodeMap[n].firstOccurence;
            int blocksubIndex = nodeMap[n].firstSubIndex;
            while (blockid != -1) {
                if (blocks[blockid]->deleteEdge(n, blocksubIndex, to))
                    return true;
                std::pair<int, int> nextIndexes = blocks[blockid]->getNextIndexes(n, blocksubIndex);
                blockid = nextIndexes.first;
                blocksubIndex = nextIndexes.second;
            }
        }
        return false;
    }

    void BlockInterface::mergeBlocks(int id, int id2) {
        while (blocks[id] == blocks[id2]) {
            std::cout<<"Same block"<<std::endl;
            id2++;
        }
        std::shared_ptr<BlockStorage> b(blocks[id]->mergeBlocks(blocks[id2].get()));
        if (b != nullptr) {
            blocks[id] = b;
            blocks[id2] = b;
            auto nodeIndexList=b->getFrontNodesInBlockWithIndex();
            for(auto x:nodeIndexList){
                nodeMap[x.first].firstOccurence=id;
                nodeMap[x.first].firstSubIndex=x.second;
            }
        }
    }

    void BlockInterface::deleteNode(int n) {
        if (nodeMap.find(n) != nodeMap.end()) {
            int blockid = nodeMap[n].firstOccurence;
            int blocksubIndex = nodeMap[n].firstSubIndex;
            while (blockid != -1) {
                blocks[blockid]->deleteNode(n,blocksubIndex);
                std::pair<int, int> nextIndexes = blocks[blockid]->getNextIndexes(n, blocksubIndex);
                blockid = nextIndexes.first;
                blocksubIndex = nextIndexes.second;
            }
            nodeMap.erase(n);
        }
    }

    bool BlockInterface::nodeAvailable(int node){
        return nodeMap.find(node)!=nodeMap.end();
    }


