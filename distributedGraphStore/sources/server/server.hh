#include<iostream>
#include<atomic>
#include<cstdio>
#include <sys/mman.h>
#include"storage/nodeDB.hh"
#include<list>
#include<map>
#include<thread>
#include"storage/graphDB.hh"
#include"configs/configHandler.hh"

using namespace std;

class Server {

protected:

    GraphDB* graphdb;

    GDBTransactionInterface* GTI;

    string rootPath;

    void initialiseDB();

    long long int completedId, snapshotId;

    vector<long long int> activeIds;

    bool active;

    long long int getId() ;

public:

    Server();

    Server(vector<string>);
    
    ~Server();

    void updateSnapshotId(long long int id) ;

    bool transactionCompleted(Transaction*t) ;

    vector<vector<string>> returnTransactionResults(Transaction*);

    void receiveTransaction(Transaction*);

    bool returnStatus() ;

    void stopServer() ;

};


