#include"server.hh"

class ServerInterface{

    Server* server;

    queue<Transaction*> tQueue;

    queue<long long int> idQueue;

    public:

    ServerInterface();

    ServerInterface(vector<string> vs);

    void setServer(Server*s);

    void acceptTransaction(Transaction* t,long long int id);
    
    bool checkTransaction(long long int id); 
    
    vector<vector<string>> sendTransactionResults(long long int id);
    
};

