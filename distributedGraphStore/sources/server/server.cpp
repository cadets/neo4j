#include"server.hh"

/*
 *  Files on disk: nodeList.dat edgeList.dat
 *  metaNodeList.dat nodeProperties.dat
 */


/*
 *
 *
 *  ServerInterface is ran from the python interface, which sends transactions to it as and when available. It implements all the security features etc.
 *  The server recieves transactions, and it sends back the results of each individual computation. 
 *  ServerInterface is an interface between the python callers and the C++ backend. 
 *
 *
 *
 */




Server::Server(){
    rootPath="db/";
    initialiseDB();
    completedId=0;
    snapshotId=0;
}

void Server::initialiseDB(){
    graphdb=new GraphDB(rootPath);
    GTI= new GDBTransactionInterface(graphdb);
    active=true;
}

vector<vector<string>> Server::returnTransactionResults(Transaction* t){
    vector<vector<string>> results=GTI->getTransactionResults(t);
    return results; 
}

Server::Server(vector<string> args){
    rootPath=args[0];
    /*
     *  Add more details on how the config handler is going to be giving the right parameters.
     *  Also, how config handler is going to be used. 
     */ 
    initialiseDB();
}
    long long int Server::getId() {
        return snapshotId;
    }

void Server::receiveTransaction(Transaction* t){
    if(this->active){
        t->setTransactionId(getId());
        GTI->addTransaction(t);
    }
}

    Server::~Server(){
        stopServer();
        delete GTI;
        delete graphdb;
    }

    void Server::updateSnapshotId(long long int id) {
        snapshotId = graphdb->nextSnapshotId();
    }

    bool Server::transactionCompleted(Transaction*t) {
        return t->getCommit();
    }
    bool Server::returnStatus() {
        return true;
    }

    void Server::stopServer() {
        active=false;
        //GTI->signalFinish();
    }


