#include"serverInterface.hh"
#include<vector>

void ServerInterface::acceptTransaction(Transaction* t, long long int id){
   tQueue.push(t);
   idQueue.push(id);
}

bool ServerInterface::checkTransaction(long long int id){
    if(idQueue.front()==id){
        return tQueue.front()->getCompleted();
    }
    return false;
}

vector<vector<string>> ServerInterface::sendTransactionResults(long long int id){
    vector<vector<string>> results;
    if(idQueue.front()==id){
        if(tQueue.front()->getSuccess()){
            results= server->returnTransactionResults(tQueue.front());
        }
        tQueue.pop();
        idQueue.pop();
    }
    return results;
}

    ServerInterface::ServerInterface(){
        server=new Server();
    }
    ServerInterface::ServerInterface(vector<string> vs){
        server= new Server(vs);
    }
    void ServerInterface::setServer(Server*s){
        server=s;
    }