#include<storage/nodeDB.hh>
#include <gtest/gtest.h>

using namespace std;

// A new one of these is created for each test

class NodeTest : public testing::Test {
public:
    NodeDbDriver* elh;

    virtual void SetUp() {
        elh = new NodeDbDriver(100);
        elh->addNode(1, 0);
        elh->addNode(2, 0);
        elh->addEdge(1, 2, 0);
    }

    virtual void TearDown() {
        delete elh;
    }
};

TEST_F(NodeTest, testNodeResults) {
    vector<int> y = {2};

    auto x = elh->getNeighbours(1, 0);
    ASSERT_EQ(x.size(), y.size()) << "Vectors x and y are of unequal length";

    for (unsigned int i = 0; i < x.size(); ++i) {
        EXPECT_EQ(x[i], y[i]) << "Vectors x and y differ at index " << i;
    }
}

//TEST_F(EdgeTest, testElementOneIsTwo)
//{
//  EXPECT_EQ(2,);
//}

TEST_F(NodeTest, testNodeNeighbours) {
    EXPECT_EQ((unsigned int) 1, elh->getNeighbours(1, 0).size());
}