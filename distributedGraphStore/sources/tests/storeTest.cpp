#include"storage/nodeDB.hh"
#include<chrono>
class Timer {
    public:
        Timer() :
            m_beg(clock_::now()) {
            }
        void reset() {
            m_beg = clock_::now();
        }

        double elapsed() const {
            return std::chrono::duration_cast<std::chrono::milliseconds>(
                    clock_::now() - m_beg).count();
        }

    private:
        typedef std::chrono::high_resolution_clock clock_;
        typedef std::chrono::duration<double, std::ratio<1> > second_;
        std::chrono::time_point<clock_> m_beg;
};
int main(int argc, char* argv[]){
    int n=1000000;
    int m=50000000;
    if(argc>2){
        n=atoi(argv[1]);
        m=atoi(argv[2]);
    }
    NodeDbDriver* dbDriver = new NodeDbDriver(100000);
    Timer tmr;

    tmr.reset();{

    for(int i=0;i<n;i++){
        dbDriver->addNode((long long int)i,0);
    }
#pragma omp parallel num_threads(4) 
        {
#pragma omp for
            for(int i =0;i<m; i++){
                dbDriver->addEdge(i%n,i,0);
            }
        }
    }
    cout<<"Tested For "<<n <<" and "<<m<<endl;
    cout<<tmr.elapsed()<<endl;

    return 0;
}
