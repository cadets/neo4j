#include"fileStore/fileHandler.hh"
#include<string>

class NodeIndex{
    FileHandler * fh;
    unordered_map<long long int, long long int> NodeIndexMap;
    public:
    
    NodeIndex(string filename){
        fh=new FileHandler(filename);
        int index=0;
        while(fh->checkIndex(index)){
            vector<char> charData=readData(index,0,8);
            char* data=reinterpret_cast<char*> (&charData[0]);
            vector<char> charData2=readData(index,8,8);
            char* data2=reinterpret_cast<char*> (&charData2[0]);
            NodeIndexMap[atol(data)]=atol(data2);            
            index++;
        }
    }

    void addNode(long long int id, long long int index){
        NodeIndexMap[id]=index;
        long long int findex=fh->getNewBlock(16);
        fh->writeData(findex,to_string(id).c_str(),8);
        fh->writeData(findex,to_string(index).c_str(),8,8);
    }
    long long int queryId(long long int id){
        if(NodeIndexMap.find(id)!=NodeIndexMap.end()){
            return NodeIndexMap[id];
        }
        return -1;
    }
}
