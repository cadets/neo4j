#include<iostream>
#include<map>
#include<unordered_set>
#include<unordered_map>
#include<vector>
#include"transaction/transaction.hh"
#include"storage/edgeHandles.hh"
#include"storage/graphDB.hh"
#include"common/commonDefs.hh"

using namespace std;

#ifndef __TRANSACTIONRUNNER__HH
#define __TRANSACTIONRUNNER__HH

enum TransactionType {
    TxEdgeRead = 4,
    TxEdgeWrite = 5,
    TxEdgeDelete = 6,
    TxNodeRead = 0,
    TxNodeWrite = 1,
    TxNodeDelete = 2,
    TxGetNeighbours = 8,
    TxGetNodeProperty = 16,
    TxGetEdgeProperty = 20
};

class operationList {
public:

    operationList(lli id, TransactionType t) {
        this->id = id;
        this->t = t;
    }
    
    lli id;
    bool deleteNode;
    //Only two operations allowed: add or delete. Update: one add and one delete. 
    TransactionType t;
};

class GraphDB;

class TransactionRunner {
    
    Transaction* tx;

    GraphDB* db;

    lli addEdge(lli from, long long int to);

    lli addNode(lli id);

    lli readNode(lli id);

    lli deleteNode(lli id);

    lli deleteEdge(lli in, lli out);

    vector<lli> readNeighbour(lli id);

    vector<lli> recordNodes;

    vector<operationList*> opList;

    unordered_set<lli> nodeReadSet; //set of nodes whose properties were read.
    unordered_set<lli> edgeReadSet; //set of nodes whose edges were read 
    unordered_set<lli> nodeWriteSet; //set of nodes which were written 
    unordered_set<lli> nodeDeleteSet;
    unordered_map<lli, vector<lli>> edgeWriteSet; //set of nodes to whom edges were added. 

    bool newNode(lli node) {
        return nodeWriteSet.find(node) == nodeWriteSet.end();
    }

    bool deletedNode(lli node) {
        return nodeDeleteSet.find(node) != nodeDeleteSet.end();
    }

    lli getId(lli node);

    vector<string> convLtoSV(vector<lli> inp) {
        vector<string> result;
        for (auto x : inp) {
            result.push_back(to_string(x));
        }
        return result;
    }

public:

    void rollBack();

    void setTransactionId(lli id) {
        tx->setTransactionId(id);
    }

    lli getTransactionId() {
        return tx->getTransactionId();
    }

    TransactionRunner(Transaction* tx = NULL, GraphDB* db = NULL) {
        this->tx = tx;
        this->db = db;
    }

    ~TransactionRunner() {
        opList.clear();
        nodeReadSet.clear();
        nodeWriteSet.clear();
        edgeReadSet.clear();
        edgeWriteSet.clear();
        tx = NULL;
        db = NULL;
    }

    vector<vector<string>> runTransaction();

    //Helper functions
    //
    TransactionType statementType(string statement);

    bool commit(vector<vector<string>>&results); 

};

#endif
