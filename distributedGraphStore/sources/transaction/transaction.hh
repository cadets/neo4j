#ifndef __TRANSACTION__HH
#define __TRANSACTION__HH

#include<iostream>
#include<vector>
#include<string>
#include<list>

using namespace std;


class Transaction{

    bool success;

    bool committed;

    bool completed;

    long long int tid;
    
    long long int snapshotId;
    
    long long int bid;

    public:
    
    /* Add this when the transaction segmentation of code comes in.
     * vector<list<int>> groups; */

    vector<string> operations;

    Transaction();

    void addStatement(string statement);

    void reorganise();

    bool getSuccess();

    void setSuccess(bool value);

    void setCommit(bool value);

    bool getCommit();

    void setCompleted(bool v);

    bool getCompleted();

    void setTransactionId(long long int id);

    long long int  getTransactionId();
    
    void setBeginId(long long int bid){
        this->bid=bid;
    }
    long long int getBeginId(){
        return bid;
    }

};
#endif
