#include"fileHandler.hh"
#include"gtest/gtest.h"

namespace{
    class FileHandlerTest: public testing::Test{
        public:
            FileHandlerTest(){
                string path="testFile.data";
                fh=new FileHandler(path);
            }
            virtual ~FileHandlerTest(){
                delete fh;
            }
            FileHandler* fh;
                long long int index;
            virtual void SetUp(){
                index=this->fh->getBlock(16);
                string data="data value";
                this->fh->writeData(index,data.c_str(),data.length());
            }
            virtual void TearDown(){
            }
    };
    TEST_F(FileHandlerTest,ReadWriteTest){
        string data="data value";
        vector<char>v=this->fh->readData(this->index,0,data.length());
        string result(v.begin(),v.end());
        ASSERT_STREQ(data.c_str(),result.c_str())<<data<<" "<<result;
    }
}
int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

