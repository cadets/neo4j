#include<server/server.hh>
#include<transaction/TransactionFactory.hh>
#include<thread>

void callThread(Transaction*t, Server*s){

    s->receiveTransaction(t);
    while(!t->getCommit())
        sleep(1);
    vector<vector<string>> result1=s->returnTransactionResults(t);
}

void testSmall(Server*s){
    Transaction* t1;
    TransactionFactory tf;
    t1=tf.createRandomWriteTransaction(11,10);
    thread tr(callThread,t1,s);
    tr.join();
    delete t1;
}
void testTwo(Server *s){
    vector<string> arguments;
    Transaction* t1,*t2;
    TransactionFactory tf;
    t1=tf.createRandomWriteTransaction(1000,1000);
    t2=tf.createRandomWriteTransaction(1000,1000);
    thread tr(callThread,t1,s);
    thread te(callThread,t2,s);

    tr.join();
    te.join();
    delete t1;delete t2;
}

void testMultiple(Server *s){
    Transaction* t1,*t2;
    TransactionFactory tf;
    t1=tf.createRandomWriteTransaction(10,10);
    t2=tf.createRandomWriteTransaction(10,10);
    thread tr(callThread,t1,s);
    thread te(callThread,t2,s);

    tr.join();
    te.join();
    delete t1;delete t2;
    Transaction* t3,*t4;
    t3=tf.createRandomReadWriteTransaction(10,20, 20);
    t4=tf.createRandomReadWriteTransaction(10,20, 20);

    thread tr1(callThread,t3,s);
    thread te1(callThread,t4,s);

    tr1.join();
    te1.join();
    
    cout<<"Multiple tested"<<endl;
  
    
    delete t3;
    delete t4;
}

int main(){
    vector<string> arguments;
    string str="~/Dropbox/Codes/distributedGraphStore/db";
    arguments.push_back(str);
    Server * s=new Server(arguments);

//    testSmall(s);
//    testTwo(s);
    testMultiple(s);
    s->stopServer();
    delete s;
    return 0;
}

